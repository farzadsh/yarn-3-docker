# Create image with all dependecy
FROM centos:centos8 as Runner

RUN mkdir -p /bm/app
WORKDIR /bm/app

ENV PATH /bm/node_modules/.bin:$PATH

# See https://stackoverflow.com/a/71309215/2795904
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

RUN dnf makecache
RUN dnf install -y gcc-c++ make tar curl

RUN curl -fsSL https://rpm.nodesource.com/setup_18.x | bash -
RUN dnf install -y nodejs
RUN corepack enable
RUN corepack prepare yarn@3.1.1 --activate

CMD [ "/bin/bash" ]
